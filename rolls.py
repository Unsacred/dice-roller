from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from random import randint
from typing import List, Dict


class Advantage(Enum):
    normal = 'N'
    advantage = 'A'
    disadvantage = 'D'


@dataclass
class Roll:
    result: int
    throws: List[int]
    advantage: Advantage


def roll_twenty(advantage: Advantage) -> Roll:
    if advantage == Advantage.normal:
        result = randint(1, 20)
        return Roll(result=result, throws=[result], advantage=Advantage.normal)
    elif advantage == Advantage.advantage:
        result1 = (randint(1, 20))
        result2 = (randint(1, 20))
        dice_result = max(result1, result2)
        return Roll(result=dice_result, throws=[result1, result2], advantage=Advantage.advantage)
    elif advantage == Advantage.disadvantage:
        result1 = (randint(1, 20))
        result2 = (randint(1, 20))
        dice_result = min(result1, result2)
        return Roll(result=dice_result, throws=[result1, result2], advantage=Advantage.disadvantage)
    else:
        raise ValueError('Advantage should be A, D or N')


def roll_twenty_to_text(rolls: List[Roll]) -> str:
    """
    2д20 с результатами 2, 20.
    1д20 с преимуществом с результатами 17(5|17).
    1д20 с помехой с результатами 2(2|20)
    """
    normal = [r for r in rolls if r.advantage == Advantage.normal]
    advantage = [r for r in rolls if r.advantage == Advantage.advantage]
    disadvantage = [r for r in rolls if r.advantage == Advantage.disadvantage]

    if normal:
        normal_text = f' {len(normal)}d20 с результатами: {", ".join([str(r.result) for r in normal])}. \n'
    else:
        normal_text = ''

    if advantage:
        advantage_text = f' {len(advantage)}d20 с преимуществом с результатами '
        for roll in advantage:
            advantage_text += f' {roll.result}({"|".join([str(t) for t in roll.throws])}),'
        advantage_text = advantage_text[: -1] + '. \n'
    else:
        advantage_text = ''

    if disadvantage:
        disadvantage_text = f' {len(disadvantage)}d20 с помехой с результатами '
        for roll in disadvantage:
            disadvantage_text += f' {roll.result}({"|".join([str(t) for t in roll.throws])}),'
        disadvantage_text = disadvantage_text[: -1] + '. \n'
    else:
        disadvantage_text = ''

    return f'{normal_text}{advantage_text}{disadvantage_text}'


def roll_to_text(roll_result: Dict[int, List[Roll]]) -> str:
    text_twenty = ''
    text_non_twenty = ''
    sum_results = 0

    for key, rolls in roll_result.items():
        if key == 20:
            text_twenty = roll_twenty_to_text(rolls)
        elif key == 100:
            text_non_twenty += f' {len(rolls)}d{key} со значениями ({",".join([str(r.result) for r in rolls])}),'
        else:
            text_non_twenty += f' {len(rolls)}d{key} со значениями ({",".join([str(r.result) for r in rolls])}),'
            sum_results += sum([r.result for r in rolls])

    if sum_results:
        text_sum = f' в сумме получилось {sum_results}.'
    else:
        text_sum = ''

    return text_twenty + text_non_twenty + text_sum


def roll(dices: List[Dict]) -> str:
    results: Dict[int, List[Roll]] = defaultdict(list)
    for dice in dices:
        value = int(dice['value'])
        if value == 20:
            advantage = dice['advantage']
            results[value].append(roll_twenty(Advantage(advantage)))
        else:
            rand = (randint(1, value))
            results[value].append(Roll(result=rand, throws=[rand], advantage=Advantage.normal))

    if 20 in results and len(results[20]) > 1 and len(results) > 1:
        return '  перемешал слишком много 20ок с другими кубами. Упс. Пожалуйста не надо так'
    return f' кинул{roll_to_text(results)}'
