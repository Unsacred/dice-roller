from random import randint


class Dice:
    def __init__(self, dice_sides, advantage):
        self.dice_sides : int = dice_sides
        self. advantage : bool = advantage



    def throw(self,dice_sides, advantage):
        if advantage:
            return max(randint(1,dice_sides), randint(1,dice_sides))
        elif advantage is False:
            return min(randint(1, dice_sides), randint(1, dice_sides))
        else:
            return randint(1, dice_sides)