document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        setTimeout(function(){
            const window_height = document.documentElement.clientHeight;
            const bag = document.querySelector('.bag');
            const sock = socket_connect();
            const room = document.querySelector('.room');
            hand_grab();
            make_roll(sock);
            x5_switch();
            clear_hand();
            room.style.height = window_height + 'px';
            const bag_rect = bag.getBoundingClientRect();
            hand.style.height = bag_rect.height + 'px';
        }, 500);
    };
};


function x5_switch() {
    const x5_button = document.querySelector('.x5_button');
    x5_button.addEventListener('click', function(ev) {
        if (x5_button.value == 'false') {
            x5_button.value = 'true';
            x5_button.style.color = 'white';
            x5_button.style.border = '1px solid white';
            x5_button.style.background = 'darkred';
        } else if (x5_button.value == 'true') {
            x5_button.value = 'false'
            x5_button.style.color = 'darkred'
            x5_button.style.border = '1px solid darkred'
            x5_button.style.background = 'white'
        };
    });
}

function add_hand_dice(hand, value, src, advantage) {
    let hand_dice = document.createElement('input');
    hand_dice.className = 'hand_dice';
    hand_dice.type = 'image';
    hand_dice.value = value;
    hand_dice.src = src;
    hand_dice.setAttribute('data-advantage', advantage);
    hand_dice.addEventListener('click', function(ev){
        hand_dice.remove()
        let hand_dice_num = document.querySelectorAll('.hand_dice');
        if (hand_dice_num == null) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '65.8%'
            };
        } else if (hand_dice_num.length <= 2) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '43.47%'
            };
        } else if (hand_dice_num.length <= 6) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '28.69%'
            };
        } else if((hand_dice_num.length > 6)) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '12.5%'
            };
        };
    });
    hand.append(hand_dice);
    const hand_dice_num = document.querySelectorAll('.hand_dice');
        if (hand_dice_num == null) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '65.8%'
            };
        } else if (hand_dice_num.length <= 2) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '43.47%'
            };
        } else if (hand_dice_num.length <= 6) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '28.69%'
            };
        } else if((hand_dice_num.length > 6)) {
            for (hand_dice of hand_dice_num) {
            hand_dice.style.width = '12.5%'
            };
        };
};

function hand_grab () {
    const dice = document.querySelectorAll(".dice");
    const hand = document.querySelector('.hand_dice_holder');
    const x5_button = document.querySelector('.x5_button');

    for (const die of dice) {
        const value = die.value;
        const src = die.src;
        const advantage = die.getAttribute('data-advantage');

        die.addEventListener('click', function(ev) {
            if (x5_button.value == 'false') {
                add_hand_dice (hand, value, src, advantage)
            } else {
                for (let i = 0; i < 5; i++) {
                    add_hand_dice (hand, value, src, advantage)
                };
            hand.scrollTop =  hand.scrollHeight
            };
        });
    };
};


function socket_connect () {
    const sock = new WebSocket('ws://' + document.domain + ':' + location.port + location.pathname);
    const messages = document.querySelector('.messages');
    const seats = document.querySelector('.seats')
    const input = document.querySelector('.input');
    const button = document.querySelector('.send');
    const players = document.querySelectorAll('.player')
    sock.onopen = function() {
        sock.send(JSON.stringify({'type': 'started'}));
    };

    sock.onmessage = function(ev) {
        const el = document.createElement('div');
        el.className = 'message'
        const messages_rect = messages.getBoundingClientRect()
        console.log (typeof messages_rect.width)
        el.style.width = messages_rect.width + 'px'
        const response = JSON.parse(ev.data);
        if (response.type == 'user_roll') {
            const str_user = response.user;
            const str_dices = response.dices
            el.textContent = str_user + ' :' + str_dices;
            messages.append(el);
        } else if (response.type == 'message') {
            const str_user = response.user;
            const str_message = response.message
            el.textContent = str_user + ': ' +  str_message;
            messages.append(el);
        } else if (response.type == 'join') {
            el.textContent = response.greeting
            messages.append(el);
            current_users(response.users);
        } else if (response.type == 'leave') {
            el.textContent = response.bye
            messages.append(el);
            current_users(response.users);
        };
    messages.scrollTop =  messages.scrollHeight
    };

    input.addEventListener('keypress', function (e) {
        if (e.key == 'Enter') {
            const data = input.value;
            sock.send(JSON.stringify({'message': data, 'type': 'message'}));
            input.value = '';
        }
    });

    sock.onclose = function() {
        sock.send(JSON.stringify({'type': 'closed'}));
    };

    sock.onerror = function() {
        window.location = '/';
    };
    window.onbeforeunload = function() {
        sock.close()
    };
    return sock;
};


function make_roll(sock){
    const roll = document.querySelector('.roll');
    roll.addEventListener('click', function(ev) {
        const hand_dice_num = document.querySelectorAll('.hand_dice')
        const roll_data = [];
        for (die of hand_dice_num) {
            roll_data.push({value: die.value, advantage: die.getAttribute('data-advantage')});
        };
        const data = JSON.stringify({'dices': roll_data, 'type': 'user_roll'});
        sock.send(data);
    });
};

function clear_hand() {
    const clear_button = document.querySelector('.clear');
    clear_button.addEventListener('click', function(ev){
        const hand_dice_num = document.querySelectorAll('.hand_dice');
        for (dice of hand_dice_num) {
            dice.remove();
        }
    });
}
function create_user_but (user) {

    const seats = document.querySelector('.seats')
    const input = document.querySelector('.input')
    const player = document.createElement('button');
    player.textContent = user.user_name;
    player.className = 'player';
    player.style.fontFamily = "'Press Start 2P', cursive";
    player.style.background = user.user_color
    seats.append(player);
    player.addEventListener('click', function(ev) {
        input.value += (' ' + player.textContent + ' ')
    });
}

function users_to_str(json_users) {
    const users = [];
    for (json_user of json_users) {
        const user = JSON.stringify(json_user).slice(1, (JSON.stringify(json_user).length - 1));
        users.push(user)
    };
    return users
}

function current_users(users) {
    const displayed_players = document.querySelectorAll('.player');
    const seats = document.querySelector('.seats');
    const displayed_players_list = [];
    const current_users = [];
    for (user of users) {
        current_users.push(user.user_name)
    };
    for (player of displayed_players) {
        displayed_players_list.push(player.textContent)
    };
    for (user of users) {
        if ((user.user_name in displayed_players_list) == false) {
            create_user_but(user)
        };
    };
    for (player of displayed_players) {
        if ((player.textContent in current_users) == false) {
            player.remove()
        };
    };
};

