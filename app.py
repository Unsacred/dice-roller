from dataclasses import dataclass
import json
import logging
import os
import random
import string
from typing import List, Dict
logging.basicConfig(level=logging.DEBUG)
from quart import Quart, render_template, websocket, session, request, redirect
from quart.wrappers.base import BaseRequestWebsocket
from rolls import roll


app = Quart(__name__)
app.secret_key = '1231231'
log = logging.getLogger(__name__)
rooms = {'': []}


@dataclass
class User:
    websock: BaseRequestWebsocket
    name: str
    color: str



@app.route('/')
async def roller():
    message = 'Приветствую на этом чудесном сайте!'
    return await render_template('index.html', message=message)


@app.route('/room/<room_id>')
async def room(room_id):
    global rooms
    if room_id not in rooms:
        return redirect('/')
    return await render_template('room.html')


@app.route('/create', methods=['POST'])
async def create():
    data = await request.form
    room_id = generate_room(rooms)
    rooms[room_id] = []
    user = data['user_name']
    if room_id not in session:
        session[room_id] = {}
    if user == '':
        message = 'Пустое имя не походит. Придумайте получше'
        return await render_template('index.html', er_message=message)
    if len(user) > 16:
        message = 'Имя должно быть меньше 16 символов'
        return await render_template('index.html', er_message=message)
    session[room_id]['user_name'] = data['user_name']
    session[room_id]['user_color'] = generate_color(rooms[room_id])
    session.modified = True
    return redirect(f'/room/{room_id}')


@app.route('/join', methods=['POST'])
async def join():
    global users
    data = await request.form
    user = data["user_name"]
    room_id = data["room_id"]
    user_name_list = [u.name for u in rooms[room_id]]
    if user == '':
        message = 'Пустое имя не походит. Придумайте получше'
        return await render_template('index.html', er_message=message)
    if len(user) > 16:
        message = 'Имя должно быть меньше 16 символов'
        return await render_template('index.html', er_message=message)
    if room_id not in session:
        session[room_id] = {}
    if 'user_name' not in session[room_id]:
        session[room_id]['user_name'] = user
        session[room_id]['user_color'] = generate_color(rooms[room_id])
    if user in user_name_list or session[room_id]['user_name'] in user_name_list:
        message = 'Имя уже занято'
        return await render_template('index.html', er_message=message)
    if room_id not in rooms:
        message = 'Такой  комнаты не существует'
        return await render_template('index.html', er_message=message)
    session.modified = True
    return redirect(f'/room/{room_id}')


def generate_room(rooms: Dict[str, List]) -> str:
    room = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
    if room in rooms:
        room = generate_room(rooms)
    return room


async def message_handling(room: List[User], user: User):
    jdata = await websocket.receive()
    data = json.loads(jdata)
    if data['type'] == 'user_roll' and (data['dices']) != []:
        result = roll(data['dices'])
        for web in room:
            await web.websock.send_json({
                'type': 'user_roll',
                'user': user.name,
                'dices': result,
            })
    elif data['type'] == 'message' and data['message'] != '':
        result = data['message']
        for web in room:
            await web.websock.send_json({
                'type': 'message',
                'user': user.name,
                'message': result,
            })
    elif data['type'] == 'started':
        result = f'{user.name} присел за стол. Приветствуем!'
        for web in room:
            await web.websock.send_json({
                'type': 'join',
                'user': user.name,
                'greeting': result,
                'users': [{'user_name': u.name, 'user_color': u.color} for u in room]  # [{'user_name': 'Bob', 'user_color': 'red'}
            })
    elif data['type'] == 'closed':
        for web in room:
            await web.websock.send_json({
                'type': 'leave',
                'user': user.name,
                'users': [{'user_name': u.name, 'user_color': u.color} for u in room],
            })
    log.debug('data received %r', data)



@app.websocket('/room/<room_id>')
async def ws(room_id):
    global rooms
    if room_id not in rooms:
        await websocket.close(404)
    else:
        user = User(
            name=session[room_id]['user_name'],
            color=session[room_id]['user_color'],
            websock=websocket._get_current_object()
        )
        rooms[room_id].append(user)
        try:
            while True:
                await message_handling(rooms[room_id], user)
        finally:
            result = f'{user.name} ушел. Пока, пока.'
            rooms[room_id].remove(user)
            for web in rooms[room_id]:
                await web.websock.send_json({
                    'type': 'leave',
                    'bye': result,
                    'users': [{'user_name': u.name, 'user_color': u.color} for u in rooms[room_id]],
                })
            if not rooms[room_id]:
                del rooms[room_id]


def generate_color(users: List[User]) -> str:
    colors = ['darkred', 'darkgreen', 'darkmagenta', 'darkcyan', 'darkorange', 'darkorchid', 'sienna', 'coral',
    'darkturquoise', 'darkkhaki', 'indigo', 'dodgerblue', 'aquamarine', 'lime']
    color = random.choices(colors)
    used_color = [u.color for u in users]
    if color in used_color:
        color = generate_color(users)
    return color


if __name__ == "__main__":
    app.run(debug=os.environ.get('DEBUG') != 'false')
